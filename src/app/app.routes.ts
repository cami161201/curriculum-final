import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./components/home/home.component";
import { DatosComponent } from "./components/datos/datos.component";
import { ContactoComponent } from "./components/contacto/contacto.component";
import { AgendaComponent } from "./components/agenda/agenda.component";
import { AgendaAgregarComponent } from "./components/agenda-agregar/agenda-agregar.component";


const APP_ROUTES: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'datos', component: DatosComponent },
  { path: 'contacto', component: ContactoComponent },
  {path:'agenda',component:AgendaAgregarComponent},
  {path:'agregar-agenda' ,component:AgendaComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);